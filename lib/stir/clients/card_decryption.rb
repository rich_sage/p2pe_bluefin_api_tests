module BluefinAPI
  class CardDecryptClient < Stir::RestClient

    self.config_file = File.join(Stir.path, 'config', "bluefin_api.yml")

    post(:decrypt) { '/Decrypt' }
    post(:ksn_decrypt) { '/KSNDecrypt'}

    response(:emv_tags) { response['EmvData']['Tags'] }
    response(:emv_serial_number) { response['EmvData']['SerialNumber'] }
    response(:track_data_value) { response['TrackData']['Value'] }
    response(:track_data_format) { response['TrackData']['Format'] }
    response(:track_data_is_contactless) { response['TrackData']['IsContactless'] }
    response(:card_data) { response['CardData'] }
    response(:decryptor_used) { response['DecryptorUsed'] }

    response(:content_length) { response['content-length'] }

    def execute_ksn_decrypt(post_body:)
      ksn_decrypt = ksn_decrypt(body: post_body)
    end
  end
end