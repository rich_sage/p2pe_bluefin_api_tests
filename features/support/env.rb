require 'cucumber'
require 'rspec'
require 'pry'
require 'pry-nav'
require 'stir/rest'
require 'faker'
require 'require_all'
require 'active_support/all'
require 'test-helpers/all'
require 'clipboard'

$project_root = File.expand_path(File.join(File.dirname(__FILE__), '..', '..'))

Stir.configure do |config|
  config.path = File.join($project_root, 'lib', 'stir')
  config.environment = 'dev'
  config.version = 'v1'
end

TestHelpers::Wait.configuration do |config|
  config.wait_timeout = 10
  config.wait_interval = 0.01
end

World(TestHelpers::Wait)

at_exit do
  STDOUT.puts 'exiting...'
end