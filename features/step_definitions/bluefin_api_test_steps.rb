And(/^I set Authorization header with MID as (.*) and MKEY as (.*)$/) do |mid, mkey|
  auth ='Bearer SPS-DEV-GW:test.'+mid+'.'+mkey
  @bankcard.set_header 'Authorization', auth
  @bankcard_url = config['bankcard_url']

end

And(/^I set url as (.*)$/) do |url|
  @bankcard.set_url url
end

And(/^I set (.*) header to (.*)$/) do |header, value|
  @bankcard.set_header header, value
end
And(/^I set url to (.*)$/) do |url|
  @bankcard.set_url url
end

And(/^I set body to (.*)$/) do |json|
  @bankcard.set_json_body json
end

And(/^I post data for transaction (.*)$/) do |path|
  @bankcard.post path
end
And(/^I patch data for transaction (.*)$/) do |path|
  @bankcard.patch path
end

And(/^response header (.*) should be (.*)$/) do |header_param, val|
  @bankcard.verify_response_headers header_param, val
end

Then(/^response body path (.*) should be (.*)$/) do |param, val|
  @bankcard.verify_response_params param, val
end

Then(/^response body path (.*) should contain (.*)$/) do |param, val|
  @bankcard.verify_response_params_contain param, val
end

And(/^I patch data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @bankcard.patch_ref path, reference
end

And(/^I post data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @bankcard.post_ref path, reference
end
And(/^I post data with existing ref num for transaction (.*)$/) do |path|
  @bankcard.post_existing_refnum path
end
And(/^I patch data with existing ref num for transaction (.*)$/) do |path|
  @bankcard.patch_existing_refnum path
end

Then(/^response code should be (.*)$/) do |responsecode|
  @bankcard.verify_response_params "code", responsecode
end

Then(/^response is empty$/) do
  @bankcard.verify_response_empty
end
Then(/^response body should be empty$/) do
  @bankcard.verify_response_empty
end